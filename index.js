
const FIRST_NAME = "Valsan";
const LAST_NAME = "Adela-Maria";
const GRUPA = "1085";

/**
 * Make the implementation here
 */
function numberParser(value) {
    if(value===Number.POSITIVE_INFINITY||value===Number.NEGATIVE_INFINITY) return NaN;
    else if(value>=Number.MAX_VALUE) return NaN;
    else if(value<=Number.MIN_VALUE) return 5;
    else if(isNaN(value)) return NaN;
    else if(typeof value==='string'|| value instanceof String)
    {
        value= value.replace(/[$,]/g,"");
        return Math.trunc(value);
    }
    else if(typeof value==='number') return Math.trunc(value);
    else if(value*2==Number.POSITIVE_INFINITY||value/10===0) return NaN;
    else return NaN;

    
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    numberParser
}

